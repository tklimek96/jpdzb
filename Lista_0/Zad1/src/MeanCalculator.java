import Model.Calculator;

import java.util.Scanner;

public class MeanCalculator {


    public static void main(String[] args) {

        System.out.println("Podaj liczby, dla których chcesz wyliczyć średnią (oddziel je przecinkiem):");

        Scanner sc = new Scanner(System.in); // standardowe wejście (System.in)
        String string = sc.nextLine();
        sc.close();

        Calculator calculator = new Calculator(string);

        String msg;
        try {
            calculator.parse();
            calculator.mean();
            calculator.round();
            msg = calculator.print(0);
        } catch (ArrayIndexOutOfBoundsException ex) {
            msg = calculator.print(1);
        } catch (NumberFormatException ex) {
            msg = calculator.print(2);
        }

        System.out.println(msg); // standardowe wyjście (System.out)


    }


}
