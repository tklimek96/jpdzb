package Model;

import java.util.stream.Stream;

public class Calculator {

    private String string;
    private double[] values;
    private double result;
    private String msg;

    public Calculator(String string) {
        this.string = string;
    }

    public void parse() {
        values = Stream.of(string.split(",")).mapToDouble(Double::parseDouble).toArray();
    }

    public void mean() {
        double sum = 0;
        for (double value : values) {
            sum += value;
        }
        result = sum / values.length;
    }

    public void round(){
        double temp = result;
        result = Math.round(temp*100.0) / 100.0;
    }

    public String print(int value) {

        switch (value) {
            case 0:
                msg = "Wartość średnia dla liczb: " + values[0] + ", " + values[1] + ", " + values[2] + " w zaokrągleniu do dwóch miejsc po przecinku wynosi: \n" + result;
                break;
            case 1:
                msg = "Wprowadź wszystkie dane!";
                break;
            case 2:
                msg = "Wprowadź poprawne wartości!";
                break;
        }
        return msg;
    }
}
