package Model;

import java.util.stream.Stream;

public class Calculator {

    private String string;
    private String y;
    private double xa, ya, xb, yb;
    private String msg;

    public Calculator(String string) {
        this.string = string;
    }

    public void parse() {
        double[] values = Stream.of(string.split(",")).mapToDouble(Double::parseDouble).toArray();
        xa = values[0];
        ya = values[1];
        xb = values[2];
        yb = values[3];
    }

    public void calculate() {
        double a;
        double b;
        String bs;

        a = (ya - yb) / (xa - xb);
        b = ya - ((ya - yb) / (xa - xb)) * xa;

        if (b >= 0) {
            bs = " + " + Double.toString(b);
        } else {
            bs = " - " + Double.toString(Math.abs(b));
        }

        y = a + "x" + bs;
    }

    public String print(int value) {

        switch (value) {
            case 0:
                msg = "Równanie prostej dla współrzędnych xA = " + xa + " yA = " + ya + " xB = " + xb + " yB = " + yb + ": \ny = " + y;
                break;
            case 1:
                msg = "Wprowadź wszystkie dane!";
                break;
            case 2:
                msg = "Wprowadź poprawne wartości!";
                break;
        }
        return msg;
    }
}
