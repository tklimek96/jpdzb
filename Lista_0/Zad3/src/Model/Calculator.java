package Model;

import java.util.stream.Stream;

public class Calculator {

    private String string;
    private double multiplier;
    private Wektor2D vector;
    private String result;
    private String msg;

    public Calculator(String string) {
        this.string = string;
    }

    public void parse() {
        double[] values = Stream.of(string.split(",")).mapToDouble(Double::parseDouble).toArray();
        vector = new Wektor2D(values[0], values[1]);
        multiplier = values[2];
    }

    public void calculate() {
        double x;
        double y;

        x = multiplier * vector.getX();
        y = multiplier * vector.getY();

        result = "(" + x + "," + y + ")";
    }

    public String print(int value) {

        switch (value) {
            case 0:
                msg = "Wynik mnożenia wektora o współrzędnych (" + vector.getX() + "," + vector.getY() + ") przez " + multiplier + " wynosi: \n" + result;
                break;
            case 1:
                msg = "Wprowadź wszystkie dane!";
                break;
            case 2:
                msg = "Wprowadź poprawne wartości!";
                break;
        }
        return msg;
    }
}
