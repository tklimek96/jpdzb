package Model;

class Wektor2D {

    private double x;
    private double y;

    Wektor2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    double getX() {
        return x;
    }

    double getY() {
        return y;
    }

}
