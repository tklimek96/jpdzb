from itertools import zip_longest


class Wielomian:

    def __init__(self, coefficients):
        try:
            self.coefficients = list(map(float, coefficients))
        except ValueError:
            print('Błąd: Podane współczynniki muszą być liczbami')
            quit()

    def degree(self):
        return len(self.coefficients) - 1

    def __str__(self):
        n = self.degree()
        polynomial = ""
        sign = ' '

        for i, coefficient in enumerate(self.coefficients):

            if coefficient >= 0:
                sign = ' + '
            if coefficient < 0 and i != 0:
                coefficient = abs(coefficient)
                sign = ' - '

            if i == 0:
                polynomial += "{}x^{}".format(coefficient, n)
            elif i == self.degree() - 1:
                polynomial += "{}{}x".format(sign, coefficient)
            elif i == self.degree():
                polynomial += "{}{}".format(sign, coefficient)
            else:
                polynomial += "{}{}x^{}".format(sign, coefficient, n)

            n -= 1

        return polynomial

    def calculate_polynomial(self, x):
        n = self.degree()
        result = 0
        for coefficient in self.coefficients:
            result += coefficient * (x ** n)
            n -= 1
        return "Wartość wielomianu dla zmiennej x = {} wynosi {}".format(x, result)

    def __add__(self, other):
        if isinstance(other, Wielomian):
            first_polynomial = self.coefficients[::-1]
            second_polynomial = other.coefficients[::-1]
            result = [sum(i) for i in zip_longest(first_polynomial, second_polynomial, fillvalue=0)]
            message = "Suma wielomianów:\n{}\noraz\n{}\nwynosi:\n{}".format(Wielomian(self.coefficients),
                                                                            Wielomian(other.coefficients),
                                                                            Wielomian(result[::-1]))
        else:
            try:
                other = float(other)
                result = self.coefficients.copy()
                result[self.degree()] += other
                message = 'Suma wielomianu:\n{}\noraz liczby\n{}\nwynosi:\n{}'.format(Wielomian(self.coefficients),
                                                                                         other, Wielomian(result))
            except ValueError:
                message = 'Błąd dodawania wielomianów: Czynnik {} musi być liczbą'.format(other)
        return message

    def __sub__(self, other):
        if isinstance(other, Wielomian):
            first_polynomial = self.coefficients[::-1]
            second_polynomial = other.coefficients[::-1]
            result = [i1 - i2 for i1, i2 in zip_longest(first_polynomial, second_polynomial, fillvalue=0)]
            message = "Różnica wielomianów:\n{}\noraz\n{}\nwynosi:\n{}".format(Wielomian(self.coefficients),
                                                                               Wielomian(other.coefficients),
                                                                               Wielomian(result[::-1]))
        else:
            try:
                other = float(other)
                result = self.coefficients.copy()
                result[self.degree()] -= other
                message = 'Różnica wielomianu:\n{}\noraz liczby\n{}\nwynosi:\n{}'.format(Wielomian(self.coefficients),
                                                                                         other, Wielomian(result))
            except ValueError:
                message = 'Błąd odejmowania wielomianów: Czynnik {} musi być liczbą'.format(other)
        return message

    def __mul__(self, other):
        if isinstance(other, Wielomian):
            first_polynomial = self.coefficients
            second_polynomial = other.coefficients
            result = [0] * (len(first_polynomial) + len(second_polynomial) - 1)

            for i, first_polynomial_coefficient in enumerate(first_polynomial):
                for j, second_polynomial_coefficient in enumerate(second_polynomial):
                    result[i + j] += first_polynomial_coefficient * second_polynomial_coefficient

            message = "Iloczyn wielomianów:\n{}\noraz\n{}\nwynosi:\n{}".format(Wielomian(self.coefficients),
                                                                               Wielomian(other.coefficients),
                                                                               Wielomian(result))
        else:
            try:
                other = float(other)
                result = [other * i for i in self.coefficients]
                message = 'Iloczyn wielomianu:\n{}\noraz liczby\n{}\nwynosi:\n{}'.format(Wielomian(self.coefficients), other, Wielomian(result))
            except ValueError:
                message = 'Błąd mnożenia wielomianów: Czynnik {} musi być liczbą'.format(other)

        return message
