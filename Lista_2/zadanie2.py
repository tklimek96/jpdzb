import sys
from pathlib import Path
from datetime import date


def rename_files(files):
    for file in files:
        try:
            new_filename = file.stem + suffix + file.suffix
            file.rename(file.with_name(new_filename))  # pathlib
            print('Zmieniono nazwę dla pliku {} na {}'.format(file, new_filename))
        except FileExistsError:
            print('Błąd: Nie można zmienić nazwy dla pliku {} - taki plik już istnieje!'.format(file))


if len(sys.argv) == 3 or len(sys.argv) == 4:
    try:
        path = str(sys.argv[1])
        file_format = str(sys.argv[2]).lower()
        try:
            suffix = '_' + sys.argv[3]
        except IndexError:
            suffix = '_' + date.today().strftime('%Y%m%d')

        directory = Path(path)

        if directory.is_dir():
            found_files = directory.rglob('*.{}'.format(file_format))  # wyszukiwanie także w podfolderach (rekurencyjnie)
            if found_files:
                rename_files(found_files)
            else:
                print('Błąd: Brak plików z rozszerzeniem .{} w podanej lokalizacji!'.format(file_format))
        else:
            print('Błąd: Podany folder nie istnieje!')

    except ValueError:
        print('Błąd: Niepoprawne wartości parametrów. Popraw dane i spróbuj jeszcze raz.')

else:
    print(
        'Błąd: Podaj DWA lub TRZY argumenty: ścieżkę do folderu, format plików oraz przyrostek (domyślnie: bieżąca data)')
