import sys
import re
from pathlib import Path
import numpy


def select_words(file):
    lines = file.readlines()
    words = []

    for line in lines:
        words_in_line = re.findall(r'\w+', line)
        words.append(words_in_line)
        for word in words_in_line:
            distance = calculate_levenshtein_distance(word, template)
            if distance <= limit:
                print({'Wyraz': word, 'Linia': lines.index(line), 'Numer wyrazu': words_in_line.index(word)})
    if len(words) == 0:
        print('We wskazanym pliku nie znaleziono żadnych słów')
        quit()


def calculate_levenshtein_distance(word, template):
    word_len = len(word)
    template_len = len(template)
    matrix = numpy.zeros((word_len + 1, template_len + 1))
    matrix[:, 0] = [i for i in range(word_len + 1)]
    matrix[0, :] = [i for i in range(template_len + 1)]

    for i in range(word_len):
        for j in range(template_len):
            if word[i] == template[j]:
                substitution_cost = 0
            else:
                substitution_cost = 1

            matrix[i + 1, j + 1] = min([matrix[i + 1, j] + 1,
                                        matrix[i, j + 1] + 1,
                                        matrix[i, j] + substitution_cost])

    return matrix[word_len, template_len]


if len(sys.argv) == 3 or len(sys.argv) == 4:
    try:
        file_name = str(sys.argv[1])
        if file_name.endswith('.txt'):
            template = str(sys.argv[2]).lower()
            try:
                limit = int(sys.argv[3])
            except IndexError:
                limit = 1
            except ValueError:
                print('Błąd! Wprowadzona maksymalnej dopuszczalna odległość musi być liczbą całkowitą!')
                quit()

            with open(Path(file_name), encoding="utf-8") as file:
                select_words(file)

        else:
            print('Błąd: Niepoprawny format pliku (oczekiwany: .txt)!')

    except ValueError:
        print('Błąd: Niepoprawne wartości parametrów. Popraw dane i spróbuj jeszcze raz.')
else:
    print(
        'Błąd: Podaj DWA lub TRZY argumenty: nazwę pliku wraz z rozszerzeniem .txt, wzorzec oraz maksymalną dopuszczalną odległość (domyślna wartość: 1)')
