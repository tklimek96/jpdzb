from zadanie3 import Wielomian

w1 = Wielomian([-3, -2, 4, 0, 5])
w2 = Wielomian([2, 1, 0, 3])

print('w1 = {}'.format(w1.__str__()), "\n")
print('Stopień wielomianu w1 = {}'.format(w1.degree()), "\n")
print(w1.calculate_polynomial(5), "\n")

print('w2 = {}'.format(w2.__str__()), "\n")
print('Stopień wielomianu w2 = {}'.format(w2.degree()), "\n")
print(w2.calculate_polynomial(-7.5), "\n")

w3 = w1 + w2
print(w3.__str__(), "\n")
w4 = w2 + w1
print(w4.__str__(), "\n")
w5 = w1 + 1
print(w5.__str__(), "\n")
w6 = w2 + 'a'
print(w6.__str__(), "\n")

w3 = w1 - w2
print(w3.__str__(), "\n")
w4 = w2 - w1
print(w4.__str__(), "\n")
w5 = w1 - 2
print(w5.__str__(), "\n")
w6 = w2 - 'b'
print(w6.__str__(), "\n")

w3 = w1 * w2
print(w3.__str__(), "\n")
w4 = w2 * w1
print(w4.__str__(), "\n")
w5 = w1 * 3
print(w5.__str__(), "\n")
w6 = w2 * 'c'
print(w6.__str__(), "\n")