J�zyki programowania:

�	Java � poziom �redniozaawansowany
�	Matlab � poziom �redni
�	SQL � poziom �redni
�	HTML � poziom �redni
�	CSS � poziom �redni
�	JavaScript � poziom podstawowy
�	Assembler � poziom podstawowy


Praca in�ynierska:

Tematem mojej pracy by�a �Aplikacja mobilna do badania s�uchu�. By� to m�j autorski pomys�, kt�ry zrealizowa�em we wsp�pracy z firm� Bioengineering. Celem by�o stworzenie prostej, przejrzystej i intuicyjnej aplikacji, dzi�ki kt�rej u�ytkownik dysponuj�c jedynie zestawem s�uchawkowym oraz smartfonem z systemem operacyjnym �Android� ma mo�liwo�� wykonania audiogramu dla obu uszu. Badanie to polega na okre�laniu poziomu s�yszenia danych cz�stotliwo�ci generowanych przez urz�dzenie. Podczas tworzenia aplikacji nauczy�em si� g��wnie w�tkowo�ci, obs�ugi b��d�w i �idiotoodporno�ci�, a w planach mam po��czenie aplikacji z serwerem, wprowadzenie systemu kont i mikrop�atno�ci oraz upload aplikacji do Play Store.


Projekty:

Na studiach wykona�em wiele projekt�w, m.in.:
�	symulacja dynamiki molekularnej (Java),
�	program do wyznaczania dopasowa� sekwencyjnych (Matlab),
�	projekt systemu bazodanowego dla apteki (SQL),
�	aplikacja mobilna korzystaj�ca z ��czno�ci Bluetooth (Android (Java)),
�	bazodanowa aplikacja webowa (Java EE, Spring).

W trakcie sta�u w Wojew�dzkim Szpitalu Specjalistycznym we Wroc�awiu, O�rodku Badawczo-Rozwojowym zrealizowa�em projekt bazodanowej aplikacji webowej do analizy danych z operacji robotem chirurgicznym Da Vinci.
