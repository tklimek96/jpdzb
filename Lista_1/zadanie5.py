import sys
import math


def message(v_1, v_2):
    if v_1 == v_2:
        string = "Podane proste są względem siebie równoległe"
    elif v_1 == -v_2:
        string = "Podane proste są względem siebie prostopadłe"
    else:
        angle = math.degrees(math.atan(abs((v_1 - v_2) / (1 + v_1 * v_2))))
        string = "Kąt między podanymi prostymi wynosi {:.2f} stopni".format(angle)
    return string


if len(sys.argv) == 3:
    try:
        a_1 = float(sys.argv[1])
        a_2 = float(sys.argv[2])
        print(message(a_1, a_2))
    except ValueError:
        print('Błąd: Podane wartości nie jest liczbąmi!')
else:
    print('Błąd: Podaj DWA argumenty: współczynniki kierunkowe prostych!')
