import math
import sys

if len(sys.argv) == 4:
    try:
        a = float(sys.argv[1])
        b = float(sys.argv[2])
        c = float(sys.argv[3])

        if a + b > c and b + c > a and a + c > b:
            s = (a + b + c) / 2
            area = math.sqrt(s * (s - a) * (s - b) * (s - c))
            print("Pole trójkąta wynosi {:.3f}".format(area))
        else:
            print('Błąd: Twoje boki nie tworzą trójkąta!')
    except ValueError:
        print("Błąd: Podane wartości nie są liczbami!")
else:
    print('Błąd: Podaj TRZY boki trójkąta!')
