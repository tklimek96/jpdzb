import sys


def read(path):
    file = open(path, encoding="utf-8")
    lines = file.readlines()
    file.close()
    data = dict()
    for line in lines:
        line = line.strip(";\n").split("\t")
        loc = line[0]
        temp = line[1]
        data.update({loc: temp})
    return data


def write(path):
    while True:
        dataset = read(path)
        location = input("Podaj nazwę miasta (lub 'end', by zakończyć):\n")
        if location.lower() == 'end':
            break
        elif location in dataset:
            print("Takie miasto już istnieje.")
        else:
            temperature = int(input("Wprowadź temperaturę dla miasta:\n"))
            file = open(path, 'a', encoding="utf-8")
            file.write('{}\t{};\n'.format(location, temperature))
            file.close()


if len(sys.argv) == 2:
    path = str(sys.argv[1])
    if path.endswith('.tsv'):
        try:
            write(path)
        except FileNotFoundError:
            print('Błąd: Nie znaleziono pliku!')
    else:
        print('Błąd: Niepoprawny format pliku (oczekiwany: .tsv)!')
else:
    print('Błąd: Podaj JEDEN argument: ścieżkę do pliku, do którego chcesz dopisać dane!')
