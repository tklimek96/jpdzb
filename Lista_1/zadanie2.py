import sys

if len(sys.argv) == 4:
    try:
        a0 = float(sys.argv[1])
        q = float(sys.argv[2])
        try:
            n = int(sys.argv[3])
            if n >= 0:
                a = a0
                data = []
                for i in range(0, n):
                    a = a * q
                    data.append(a)
                formatted_data = ["%.3f" % m for m in data]
                print('Uzyskany ciąg: \n', formatted_data)
            else:
                print('Błąd: Argument n musi być mieć wartość dodatnią!')
        except ValueError:
            print("Błąd: Argument n musi być liczbą całkowitą!")
    except ValueError:
        print("Błąd: Podane wartości nie są liczbami!")
else:
    print('Błąd: Podaj TRZY argumenty: wyraz początkowy a[0], iloraz q oraz liczbę elementów do obliczenia n')
