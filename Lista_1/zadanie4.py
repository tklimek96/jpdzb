import sys


def count_vowels(word):
    count = 0
    vowels = "AEIOUĄĘÓaeiouąęó"

    for i in word:
        if i in vowels:
            count = count + 1
    return count


if len(sys.argv) == 2:
    string = str(sys.argv[1])
    if len(string) > 1:
        print("{}: {}".format(string, count_vowels(string)))
    else:
        print('Błąd: Podany argument jest za krótki, by być wyrazem!')

else:
    print('Błąd: Podaj JEDEN argument: wyraz, dla którego mają być zlcizone samogłoski!')
