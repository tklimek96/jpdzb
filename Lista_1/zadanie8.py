import sys
import zadanie8_Module

if len(sys.argv) == 3:
    path = str(sys.argv[1])
    mode = str(sys.argv[2])
    if path.endswith('.tsv'):
        try:
            if mode.lower() == "odczyt":
                zadanie8_Module.sort_and_print(zadanie8_Module.read(path))
            elif mode.lower() == "zapis":
                zadanie8_Module.write(path)
            else:
                print("Błąd: Nieprawidłowa nazwa trybu pracy z plikiem (odczyt/zapis)!")
        except FileNotFoundError:
            print('Błąd: Nie znaleziono pliku!')
    else:
        print('Błąd: Niepoprawny format pliku (oczekiwany: .tsv)!')
else:
    print('Błąd: Podaj DWA argumenty: ścieżkę do pliku oraz tryb pracy z plikiem (odczyt/zapis)!')
