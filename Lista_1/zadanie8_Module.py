def read(path):
    file = open(path, encoding="utf-8")
    lines = file.readlines()
    file.close()
    data = dict()
    for line in lines:
        line = line.strip(";\n").split("\t")
        loc = line[0]
        temp = line[1]
        data.update({loc: temp})
    return data


def sort_and_print(data):
    sorted_data = sorted(data.items(), reverse=True, key=lambda x: x[1])
    for location, temperature in sorted_data:
        print('{}: {}'.format(location, temperature))
    return sorted_data


def write(path):
    while True:
        dataset = read(path)
        location = input("Podaj nazwę miasta (lub 'end', by zakończyć):\n")
        if location.lower() == 'end':
            break
        elif location in dataset:
            print("Takie miasto już istnieje.")
        else:
            temperature = int(input("Wprowadź temperaturę dla miasta:\n"))
            file = open(path, 'a', encoding="utf-8")
            file.write('{}\t{};\n'.format(location, temperature))
            file.close()
