import sys


def read(path):
    file = open(path, encoding="utf-8")
    lines = file.readlines()
    file.close()
    data = dict()
    for line in lines:
        line = line.strip(";\n").split("\t")
        loc = line[0]
        temp = line[1]
        data.update({loc: temp})
    return data


def sort_and_print(data):
    sorted_data = sorted(data.items(), reverse=True, key=lambda x: x[1])
    for location, temperature in sorted_data:
        print('{}: {}'.format(location, temperature))


if len(sys.argv) == 2:
    path = str(sys.argv[1])
    if path.endswith('.tsv'):
        try:
            sort_and_print(read(path))
        except FileNotFoundError:
            print('Błąd: Nie znaleziono pliku!')
    else:
        print('Błąd: Niepoprawny format pliku (oczekiwany: .tsv)!')
else:
    print('Błąd: Podaj JEDEN argument: ścieżkę do pliku z danymi!')
