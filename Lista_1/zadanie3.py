import sys


def table(number):
    data = [[i * j for j in range(1, number + 1)] for i in range(1, number + 1)]  # generator zagnieżdżony
    return data


if len(sys.argv) == 2:
    try:
        value = int(sys.argv[1])
        if value > 1:
            for row in table(value):
                print(*row, sep='\t')
        else:
            print('Błąd: Podana maksymalna liczba n do obliczneia tabliczki mnożenia musi być większa od 1!')

    except ValueError:
        print("Błąd: Podana wartość nie jest liczbą całkowitą!")
else:
    print('Błąd: Podaj JEDEN argument: maksymalną liczbę n do obliczenia tabliczki mnożenia')
