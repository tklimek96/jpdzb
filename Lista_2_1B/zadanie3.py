import unittest
from parameterized import parameterized_class
from wielomian import Wielomian


@parameterized_class('coeffs', [
    ([1, 2, 3],),
    ([0, 0, 0],),
    ([0],),
    ([7.536, 0.655],),
    ([-45.65, 9595.64],),
    ([598.7, -7788.782],),
    ([-955.56, -125.55],)
])
class TestCreatingPolynomials(unittest.TestCase):
    def test_creating_polynomials(self):
        self.assertIsInstance(Wielomian(self.coeffs), Wielomian)


@parameterized_class(('coeffs', 'x_value', 'expected_value'), [
    ([9, 8, 7], 3, 112),
    ([5, 67, -8], 3, 238),
    ([7, -5, 1], -8, 489),
    ([-4, 5, 6], 2, 0),
    ([0, 0, 0], 1, 0),
    ([5, 4, 3], 0, 3),
    ([0], 7, 0),
    ([0, 0, 0], 5, 0),
    ([3], 2, 3),
    ([65, 5, 684, 54, 5, 78, 9], 32, 70679993801),
])
class TestCalculatingIntegers(unittest.TestCase):
    def test_calculating_integers(self):
        self.assertEqual(Wielomian(self.coeffs).oblicz_wartosc(self.x_value), self.expected_value)


@parameterized_class(('coeffs', 'x_value', 'expected_value', 'tolerance'), [
    ([4.64, 8.484, 0.584, 10.65], 7.454, 2408.09459, 3.0950404834584333e-06),
    ([9.564, 4.45, 78.45, 24.555754], -99.547, -9398300.299812, 3.9674341678619385e-07),
    ([-79.98, -9.95, -74.05, -6.4], 2.5848, -1645.500308011, 3.560671757441014e-10),
    ([-6.67, -44.21, -96.5, -44.2], -11.501, 5364.740, 7.158833068388049e-05),
])
class TestCalculatingFloats(unittest.TestCase):
    def test_calculating_floats(self):
        self.assertAlmostEqual(Wielomian(self.coeffs).oblicz_wartosc(self.x_value), self.expected_value,
                               delta=self.tolerance)


@parameterized_class('coeffs', [
    ([],),
])
class TestValueError(unittest.TestCase):
    def test_value_error(self):
        with self.assertRaises(ValueError):
            Wielomian(self.coeffs)


@parameterized_class('coeffs', [
    (['!qwerty'],),
    ([9, 8, 7, 'zxcvbnm'],)
])
class TestTypeError(unittest.TestCase):
    def test_type_error(self):
        with self.assertRaises(TypeError):
            Wielomian(self.coeffs)


if __name__ == '__main__':
    unittest.main()
