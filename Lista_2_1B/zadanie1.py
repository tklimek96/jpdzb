import unittest
from parameterized import parameterized_class
import odejmij as tested_class


@parameterized_class(('x', 'y', 'expected_result'), [
    (0, 0, 0),
    (5, 5, 0),
    (5, 3, 2),
    (3, 5, -2),
    (-5, 3, -8),
    (-3, -5, 2),
    (-5, -2, -3),
    (-5, -5, 0),
    (5, -3, 8)
])
class TestSub(unittest.TestCase):
    def test_sub(self):
        self.assertEqual(tested_class.odejmij(self.x, self.y), self.expected_result)


if __name__ == '__main__':
    unittest.main()


