class Wielomian:
    def __init__(self, coeffs):
        if not coeffs:
            raise ValueError('Lista wspolczynnikow nie moze byc pusta.')

        try:
            coeffs = [float(c) for c in coeffs]
            self.coeffs = coeffs
        except ValueError:
            raise TypeError('Wszystkie wspolczynniki musza być liczbami.')

    # def oblicz_wartosc(self, x):
    #     wartosc = 0
    #     for i in range(len(self.coeffs)):
    #         wartosc += self.coeffs[i] * x ** (len(self.coeffs) - 1 - i)
    #     return wartosc

    def oblicz_wartosc(self, x):
        wartosc = self.coeffs[0]
        for i in range(len(self.coeffs)-1):
            wartosc = wartosc*x+self.coeffs[i+1]
        return wartosc


