import unittest
from parameterized import parameterized_class
import odejmij as tested_class


@parameterized_class(('x', 'y', 'expected_result', 'tolerance'), [
    (1.1, 1, 0.1, 8.326672684688674e-17),
    (123456789.123, 123456789.123456, -0.000456, 5.3348541259514576e-09),
    (498845.6484, 8645486.484, -8146640.8356, 9.313225746154785e-10),
    (325.61541, -18156.152, 18481.76741, 3.637978807091713e-12),
    (445.54877, 89958.489887, -89512.941117, 1.4551915228366852e-11)
])
class TestSub(unittest.TestCase):
    def test_sub(self):
        self.assertAlmostEqual(tested_class.odejmij(self.x, self.y), self.expected_result, delta=self.tolerance)


if __name__ == '__main__':
    unittest.main()