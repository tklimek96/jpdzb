from Zad2.Order import Order, TooManyTicketsError
import unittest


class OrderTests(unittest.TestCase):
    def test_adding_tickets(self):
        order = Order()
        ticket_1 = ('FULL', 200)
        ticket_2 = ('HALF', 100)
        order.add_ticket(ticket_1)
        order.add_ticket(ticket_2)
        self.assertEqual(ticket_1, order.get_tickets()[0])
        self.assertEqual(ticket_2, order.get_tickets()[1])

    def test_too_many_tickets_added(self):
        order = Order()
        ticket = ('FULL', 200)
        for t in range(4):
            order.add_ticket(ticket)
        with self.assertRaises(TooManyTicketsError):
            order.add_ticket(ticket)

    def test_summary(self):
        order = Order()
        ticket_1 = ('FULL', 200)
        ticket_2 = ('HALF', 100)
        expected_summary = "Order:" \
                           "\n\tNo tickets added." \
                           "\n Total: 0.00 PLN"
        self.assertEqual(expected_summary, order.get_summary())
        order.add_ticket(ticket_1)
        expected_summary = "Order:" \
                           "\n\tType: FULL\tAmount: 1\tPrice: 200.00 PLN" \
                           "\nTotal: 200.00 PLN"
        self.assertEqual(expected_summary, order.get_summary())
        order.add_ticket(ticket_2)
        expected_summary = "Order:" \
                           "\n\tType: FULL\tAmount: 1\tPrice: 200.00 PLN" \
                           "\n\tType: HALF\tAmount: 1\tPrice: 100.00 PLN" \
                           "\nTotal: 300.00 PLN"
        self.assertEqual(expected_summary, order.get_summary())
        order.add_ticket(ticket_1)
        expected_summary = "Order:" \
                           "\n\tType: FULL\tAmount: 2\tPrice: 400.00 PLN" \
                           "\n\tType: HALF\tAmount: 1\tPrice: 100.00 PLN" \
                           "\nTotal: 500.00 PLN"
        self.assertEqual(expected_summary, order.get_summary())
        order.add_ticket(ticket_2)
        expected_summary = "Order:" \
                           "\n\tType: FULL\tAmount: 2\tPrice: 400.00 PLN" \
                           "\n\tType: HALF\tAmount: 2\tPrice: 200.00 PLN" \
                           "\nTotal: 600.00 PLN"
        self.assertEqual(expected_summary, order.get_summary())


if __name__ == '__main__':
    unittest.main()
