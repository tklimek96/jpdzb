from Zad1.ReportGenerator import ReportGenerator
import unittest
from unittest.mock import patch


class ReportGeneratorTestCase(unittest.TestCase):

    @patch('Zad1.ReportGenerator.os.path')
    def test_folder_exists(self, mock_path):
        mock_path.isdir.return_value = True
        rep_gen = ReportGenerator('/TestDirectory')
        with self.assertRaises(FileExistsError):
            rep_gen.create_folder()

    @patch('Zad1.ReportGenerator.os.path')
    @patch('Zad1.ReportGenerator.os')
    def test_create_folder(self, mock_os, mock_path):
        mock_path.isdir.return_value = False
        rep_gen = ReportGenerator('/TestDirectory')
        rep_gen.create_folder()
        self.assertTrue(mock_os.mkdir.called)


if __name__ == '__main__':
    unittest.main()
