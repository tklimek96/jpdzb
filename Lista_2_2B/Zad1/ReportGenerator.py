import os


class ReportGenerator:
    def __init__(self, path):
        self.dir_path = path

    def check_folder(self):
        return os.path.isdir(self.dir_path)

    def create_folder(self):
        os.mkdir(self.dir_path)
